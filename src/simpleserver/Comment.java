package simpleserver;

import java.util.HashMap;
import java.util.Map;

/**
 * In this method the Java object is stored which is obtained by converting the JSON file
 */
public class Comment implements Data {
    public static Map<Integer, Comment> getPostidDictc() {
        return postidDictc;
    }

    private final static Map<Integer, Comment> postidDictc = new HashMap<Integer, Comment>();
   // private final static Map<Integer, Comment> useridDict = new HashMap<Integer, Comment>();

    private String data;
    private int userid;
    private int postid;

    public Comment(String data, int userid, int postid){
        this.data = data;
        this.userid = userid;
        this.postid = postid;


       // useridDict.put(userid, this);
          postidDictc.put(postid, this);
    }

    public Comment() {

    }


/*    public static Comment getUser(int userid){
        return useridDict.get(userid);
    }*/

      public static Comment getComment(int postid){
        return postidDictc.get(postid);
    }
}
