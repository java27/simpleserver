package simpleserver;

import java.util.HashMap;
import java.util.Map;
/**
 * In this method the Java object is stored which is obtained by converting the JSON file
 */

public class Posts implements Data  {

    private String data;
    private int userid;
    private int postid;




    private final static Map<Integer, Posts> postidDict = new HashMap<>();
    //private final static Map<Integer, Posts> useridDict = new HashMap<>();

    public static Map<Integer, Posts> getPostidDict() {
        return postidDict;
    }

    public Posts() {

    }

    public Posts(String data, int userid, int postid){
        this.data = data;
        this.userid = userid;
        this.postid = postid;


       // useridDict.put(userid, this);
        postidDict.put(postid, this);
    }

    /*public static Posts getPosts(int userid){
        return useridDict.get(userid);
    }*/

   public static Posts getPosts(int postid){
        return postidDict.get(postid);
    }


}
