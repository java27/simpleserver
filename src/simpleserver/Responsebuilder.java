package simpleserver;


import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import com.google.gson.Gson;


import javax.xml.ws.Response;

/**
 * This is a Responsebuilder class and all the methods are setting the value and it is sent to the Response class
 */

public  class Responsebuilder {
    Gson gson = new Gson();
    Data[] data;
    String status = null;
    int timestamp = 0;
    int entries = 0;
    String sdf;


   public void setResponsecode (String j){
        this.status = j;

    }

    public void setdata(Data[] data ){

        this.data =  data;

        //this.data[0] = data;


    }

    public void setTimestamp(){
       this.sdf =  new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());


    }

    public String build()
    {

        simpleserver.Response response = new simpleserver.Response(status, sdf, entries,data);
        String result = gson.toJson(response);
        return result;

    }




    public void setEntries(Data[] data){

        this.entries = data.length;

    }

    public void setdata(Data data ){
        this.data = new Data[1];
        this.data[0] = data;
        this.entries = 1;


    }

}
