package simpleserver;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import jdk.internal.dynalink.beans.StaticClass;
// trial start

//trial end

public class Server {
    private static Server ourInstance = new Server();
     static int useridk = 0;
     public static User[] users;
    public static Posts[] posts;
    public static Comment[] comments;
    public static Server getInstance() {
        return ourInstance;
    }

    private Server() {
    }

    /**
     * In this main method we are converting gson.fromJson and storing it in particular class. We are also parsing the response line
     * we received from the client and according to the keywords used by the client we are selecting which data needs to be returned from Json file.
     * Selection is done via factory method and the response is build using response builder.
     * @param args
     */

    public static void main(String[] args)
    {
        ServerSocket ding;
        Socket dong = null;
        String resource = null;
        Gson gson = new Gson();
        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader("src/data.json"));
            JsonParser jsonParser = new JsonParser();
            JsonObject obj = jsonParser.parse(br).getAsJsonObject();

            Server.users = gson.fromJson(obj.get("users"), User[].class);
            posts = gson.fromJson(obj.get("posts"), Posts[].class);
            //final Map<Integer, Posts> postidkDict = new HashMap<>();
            comments = gson.fromJson(obj.get("comments"), Comment[].class);
            //final Map<Integer, Posts> commentidkDict = new HashMap<>();
            String jsonString = gson.toJson(users);
            String jsonStringp = gson.toJson(posts);
            String jsonStringc = gson.toJson(comments);
            System.out.println(jsonString);
            System.out.println(jsonStringp);
            System.out.println(jsonStringc);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            ding = new ServerSocket(1299);
            System.out.println("Opened socket " + 1299);
            while (true) {

                // keeps listening for new clients, one at a time
                try {
                    dong = ding.accept(); // waits for client here
                } catch (IOException e) {
                    System.out.println("Error opening socket");
                    System.exit(1);
                }

                InputStream stream = dong.getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(stream));
                String jString = null;
                String jsonResponse = null;
                try {

                    // read the first line to get the request method, URI and HTTP version
                    String line = in.readLine();
                    System.out.println("----------REQUEST START---------");
                    System.out.println(line);
                    // trial

                    String k[] = line.split(" ");
                    String j[] = k[1].split("[?]");
                    String ok = j[0];
                    //  Data data = ok;
                    int b = 0;
                    int n = 0;
                    String param1;


                    int params = -1;
                    if (j.length > 1) {

                        String l[] = j[1].split("&");
                        String m[] = l[0].split("=");
                        param1 = m[0];
                        //System.out.println(param1);
                        params = Integer.parseInt(m[1]);


                    }

                    Processor Process = ProcessFactory.getProcessor(ok);
                    jsonResponse = Process.run(params);


                    //trial end
                    // read only headers

                    line = in.readLine();
                    while (line != null && line.trim().length() > 0) {
                        int index = line.indexOf(": ");
                        if (index > 0) {
                            System.out.println(line);
                        } else {
                            break;
                        }
                        line = in.readLine();
                    }
                    System.out.println("----------REQUEST END---------\n\n");
                } catch (IOException e) {
                    System.out.println("Error reading");
                    System.exit(1);
                }

                BufferedOutputStream out = new BufferedOutputStream(dong.getOutputStream());
                PrintWriter writer = new PrintWriter(out, true);  // char output to the client

                // every response will always have the status-line, date, and server name
                writer.println("HTTP/1.1 200 OK");
                writer.println("Server: TEST");
                writer.println("Connection: close");
                writer.println("Content-type: application/json");
                writer.println("");


                // Body of our response
                //writer.println("<h1>Hello Kafe!</h1>");
                writer.println(jsonResponse);

                dong.close();
            }
        } catch (IOException e) {
            System.out.println("Error opening socket");
            System.exit(1);
        }
    }
}

