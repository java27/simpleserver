package simpleserver;

/**
 * This class is an example of factory design pattern and this returns instances of new class.
 */
public class ProcessFactory {
    public static Processor getProcessor(String request)
    {

       switch(request)
       {
           case "/users":

             return new UserProcessor();

          case "/posts":
               return new PostProcessor();

           case "/comments":
               return new CommentProcessor();

       }
       return null;
    }
}
