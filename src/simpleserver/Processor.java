package simpleserver;

/**
 * This is an interface which is implemented by UserProcessor, PostProcessor and CommentProcessor class. so that they can override
 * the same method.
 */
public interface Processor {

   String run(int j);


}

