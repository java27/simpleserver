package simpleserver;

import java.util.HashMap;
import java.util.Map;

/**
 * In this method the Java object is stored which is obtained by converting the JSON file
 */
public class User implements Data {

    private String username;
    private int userid;
    private final static Map<Integer, User> useridDict = new HashMap<>();

    public static Map<Integer, User> getUseridDict() {
        return useridDict;
    }

    public User(){

    }
    public User(String username, int userid){
        this.username = username;
        this.userid = userid;

        useridDict.put(userid, this);
    }


    public User getUser(int userid){
        return useridDict.get(userid);
    }


}
